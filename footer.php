<?php
/**
 * The template for displaying the footer
 *
 * @package WordPress
 * @subpackage Vlogger
 * @since Vlogger 1.0
 */
?>

<footer id="footer" class="container-fluid">
	
	<?php get_sidebar('bottom'); ?>
	
	<div class="row">
		<div class="copyright">
			
			<?php 
				
				if(get_theme_mod('vlogger_footer_text')) {
					echo get_theme_mod('vlogger_footer_text');
				} else {
					vlogger_copyright();
				} 
			
			?>
				
		</div>
	</div>
</footer>

<?php wp_footer(); ?>
</body>
</html>