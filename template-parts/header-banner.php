<?php
/**
 * The template part for displaying custom-header
 * - Logo + Banner
 *
 * @package WordPress
 * @subpackage Vlogger
 * @since Vlogger 1.0.4
 */
 ?>
 
 		<div class="row site-branding-wrapper">
			<div class="container">
				<div class="row">
					<!-- Start Site Branding -->
					<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
						<div class="row">
							<?php
							
								$image = wp_get_attachment_image_src( get_theme_mod( 'custom_logo' ) , 'full' );
								
								if ( $image[0] ) :
									
							?>
							<a class="logo" href="<?php echo esc_url(home_url('/')); ?>" title="<?php echo esc_attr(get_bloginfo('name', 'display')); ?>" rel="home">
								<img src="<?php echo $image[0]; ?>" alt="<?php echo esc_attr(get_bloginfo('name', 'display')); ?>" />
							</a>
							
							<?php

							else :
							?>
							
								<?php if(get_header_textcolor()!='blank') { ?>
								<span class="site-title">
									<a class="logo" href="<?php echo esc_url(home_url('/')); ?>" style="color:#<?php echo get_header_textcolor(); ?>!important;" title="<?php echo esc_attr(get_bloginfo('name', 'display')); ?>" rel="home">
										<?php bloginfo('name'); ?>
									</a>
								</span>
								<?php } ?>
							
							<?php

							endif;
							?>
							
							<?php
							$description = get_bloginfo( 'description', 'display' );
							if ( $description || is_customize_preview() ) : ?>
								<span class="site-description"><?php echo $description; ?></span>
							<?php endif; ?>
							
						</div>
					</div>
					<!-- End Site Branding -->
					
					<!-- Start Banner -->
					<div class="col-lg-9 col-md-9 col-sm-12 col-xs-12 header-banner-wrapper">
						<div class="row">
							<?php if(get_theme_mod('vlogger_header_banner')) : ?>
								<a href="<?php echo esc_url(get_theme_mod('vlogger_header_banner_link', '#')); ?>">
									<img class="header-banner" src="<?php echo esc_url(get_theme_mod('vlogger_header_banner')); ?>" alt="Header Banner Image" width="" height="" />
								</a>
							<?php endif; ?>
						</div>
					</div>
					
				</div>
			</div>
		</div>