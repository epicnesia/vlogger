<?php
/**
 * The template part for displaying custom-header
 * - Header Image
 *
 * @package WordPress
 * @subpackage Vlogger
 * @since Vlogger 1.0.4
 */
?>

<!-- Start Site Branding -->
<div class="row">
	<div class="container-fluid">
		<div class="row">
			<div class="col-lg-12">
				<div class="row">

					<div class="centered">
						<a class="logo" href="<?php echo esc_url(home_url('/')); ?>" title="<?php echo esc_attr(get_bloginfo('name', 'display')); ?>" rel="home"> <img src="<?php header_image(); ?>"  height="<?php echo get_custom_header() -> height; ?>" width="<?php echo get_custom_header() -> width; ?>" alt="<?php bloginfo('name'); ?>"/> </a>
					</div>

				</div>
			</div>
		</div>
	</div>
</div>
<!-- End Site Branding -->