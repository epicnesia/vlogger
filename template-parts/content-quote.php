<?php
/**
 * The template part for displaying content
 * Quote Post Format
 *
 * @package WordPress
 * @subpackage Vlogger
 * @since Vlogger 1.0
 */

?>

<div id="post-<?php the_ID(); ?>" <?php post_class(vlogger_class_col(is_singular())); ?>>
	<?php
	
	if ( has_post_thumbnail() ) {
		
	?> 
	    <div class="post-content" style="background: url('<?php the_post_thumbnail_url(vlogger_thumbnail_name()); ?>') no-repeat center center;">
	<?php

	} else {
	?>
		<div class="post-content">
	<?php

	}
	?>
		<article class="post-article">
			<blockquote><?php the_content(); ?></blockquote>
			<h5 class="qoute-title"><?php the_title(); ?></h5>
		</article>
	</div>
</div>