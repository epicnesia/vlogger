<?php
/**
 * The template part for displaying single posts
 *
 * @package WordPress
 * @subpackage Vlogger
 * @since Vlogger 1.0
 */

?>

<div id="post-<?php the_ID(); ?>" <?php post_class(vlogger_class_col(is_singular())); ?>>
	<?php

	if (has_post_thumbnail()) {
		the_post_thumbnail();
	}
	?>
	<div class="post-content">
		<header class="post-header">

			<?php vlogger_category_list(); ?>

			<h3 class="post-title"><?php the_title(); ?></h3>

			<?php vlogger_post_meta(); ?>
		</header>
		<article class="post-article">
			<?php the_content(); ?>

			<?php

			wp_link_pages(array('before' => '<div class="page-links">' . esc_html__('Pages:', 'vlogger'), 'after' => '</div>', 'link_before' => '<span>', 'link_after' => '</span>', 'pagelink' => '%', 'echo' => 1));
			?>
		</article>
		<footer class="post-footer">
			<?php vlogger_tags_list(); ?>
			<?php if ( get_edit_post_link() ) :
			?>
			<div class="edit-link">
				<?php
				edit_post_link(sprintf(
				/* translators: %s: Name of current post */esc_html__('Edit %s', 'vlogger'), the_title('<span class="screen-reader-text">"', '"</span>', false)), '<i class="fa fa-pencil-square-o"></i><span>', '</span>');
				?>
			</div>
			<?php endif; ?>
		</footer>
	</div>
	<?php if(get_theme_mod('vlogger_show_author_box') == 1) :
	?>
	<div class="author-box">
		<?php
		if ('' !== get_the_author_meta('description')) {
			get_template_part('template-parts/biography');
		}
		?>
	</div>
	<?php endif; ?>
</div>
