<?php
/**
 * The template for displaying search results pages
 * - Left sidebar layout
 *
 * @package WordPress
 * @subpackage Vlogger
 * @since Vlogger 1.0.4
 */

$active_sidebar = is_active_sidebar('vlogger-primary-sidebar'); ?>

			<!-- Start Left Sidebar Layout -->
					
				<?php if($active_sidebar) : ?>
					
				<aside class="col-lg-4 left">
					<?php get_sidebar(); ?>
				</aside>
					
				<?php endif; ?>
				
				<div class="<?php echo $active_sidebar ? 'col-lg-8 col-md-12 col-sm-12 col-xs-12 right' : 'col-lg-12 col-md-12 col-sm-12 col-xs-12'; ?>">
					
					<?php 
						
					if ( have_posts() ) : 
							
					?>
					
					<header class="archive-header">
						<h2 class="page-title">
							<?php printf( __( 'Search Results for: %s', 'vlogger' ), '<span>' . esc_html( get_search_query() ) . '</span>' ); ?>
						</h2>
					</header><!-- .page-header -->
					<div class="row grid">
			
						<?php
						
							// Start the loop.
							while ( have_posts() ) : the_post();
							
							/*
							 * Include the Post-Format-specific template for the content.
							 * If you want to override this in a child theme, then include a file
							 * called content-___.php (where ___ is the Post Format name) and that will be used instead.
							 */
							get_template_part( 'template-parts/content', 'search' );
							
							endwhile;

						// If no content, include the "No posts found" template.
						else :
							
							get_template_part( 'template-parts/content', 'none' );
								
						endif;
					?>
					</div>
					
					<?php if(get_the_posts_pagination()): ?>
						<div class="pagination">
							<?php echo paginate_links(); ?>
						</div>
					<?php endif; ?>
					
				</div>
				
			<!-- End Left Sidebar Layout -->