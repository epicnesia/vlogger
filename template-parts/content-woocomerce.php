<?php
/**
 * The template for displaying woocomerce content
 *
 * @package WordPress
 * @subpackage Vlogger
 * @since Vlogger 1.0
 */
?>

<div id="post-<?php the_ID(); ?>" <?php post_class('col-lg-12'); ?>>

	<div class="post-content">

		<article class="post-article">
			<?php woocommerce_content(); ?>
		</article>
		<footer class="post-footer">
			<?php if ( get_edit_post_link() ) :
			?>
			<div class="edit-link">
				<?php
				edit_post_link(sprintf(
				/* translators: %s: Name of current post */esc_html__('Edit %s', 'vlogger'), the_title('<span class="screen-reader-text">"', '"</span>', false)), '<i class="fa fa-pencil-square-o"></i><span>', '</span>');
				?>
			</div>
			<?php endif; ?>
		</footer>
	</div>
</div>
