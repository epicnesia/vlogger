<?php
/**
 * The template part for displaying content
 * Status Post Formats
 * - Left featured image layout
 *
 * @package WordPress
 * @subpackage Vlogger
 * @since Vlogger 1.0.4
 */
 ?>
 
 <div id="post-<?php the_ID(); ?>" <?php post_class(vlogger_class_col(is_singular())); ?>>
	<div class="post-list">
		<?php

		if ( has_post_thumbnail() ) {
		?>
		<div class="post-list-thumb col-lg-6">
			<div class="row">
				<?php

				the_post_thumbnail(vlogger_thumbnail_name());
				?>
			</div>
		</div>
		<?php
		}
		?>
		<div class="post-content <?php echo has_post_thumbnail() ? 'col-lg-6' : ''; ?>">
			<header class="post-header">

				<?php vlogger_post_meta(); ?>
			</header>
			<article class="post-article">
				<?php the_content(); ?>

				<?php

				wp_link_pages(array('before' => '<div class="page-links">' . esc_html__('Pages:', 'vlogger'), 'after' => '</div>', 'link_before' => '<span>', 'link_after' => '</span>', 'pagelink' => '%', 'echo' => 1));
				?>
			</article>
		</div>
	</div>
</div>