<?php
/**
 * The template part for displaying content
 * Status Post Formats
 *
 * @package WordPress
 * @subpackage Vlogger
 * @since Vlogger 1.0
 */

$post_layout = get_theme_mod('vlogger_posts_layout');

// List with left image
if($post_layout == '3') :
	
	get_template_part('template-parts/content-'.get_post_format(), 'left-img');

// List with right image
elseif($post_layout == '4') :
	
	get_template_part('template-parts/content-'.get_post_format(), 'right-img');

// Default and grid layout
else :

?>

<div id="post-<?php the_ID(); ?>" <?php post_class(vlogger_class_col(is_singular())); ?>>
	<?php

	if (has_post_thumbnail()) {
		the_post_thumbnail(vlogger_thumbnail_name());
	}
	?>
	<div class="post-content">
		<header class="post-header">
			<?php vlogger_post_meta(); ?>
		</header>
		<article class="post-article">
			<?php the_content(); ?>

			<?php

			wp_link_pages(array('before' => '<div class="page-links">' . esc_html__('Pages:', 'vlogger'), 'after' => '</div>', 'link_before' => '<span>', 'link_after' => '</span>', 'pagelink' => '%', 'echo' => 1));
			?>
		</article>
	</div>
</div>

<?php

endif;
