<?php
/**
 * The template part for displaying content
 * Link Post Format
 *
 * @package WordPress
 * @subpackage Vlogger
 * @since Vlogger 1.0
 */
?>

<div id="post-<?php the_ID(); ?>" <?php post_class(vlogger_class_col(is_singular())); ?>>
	<a href="<?php echo esc_url(get_the_content()); ?>" title="<?php the_title_attribute(); ?>">
	<div class="post-content">
		<article class="post-article">
			<?php the_title(); ?>
		</article>
	</div> </a>
</div>