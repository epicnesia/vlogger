<?php
/**
 * The template part for displaying content
 * index.php
 * 
 * - Full width layout
 *
 * @package WordPress
 * @subpackage Vlogger
 * @since Vlogger 1.0.4
 */
?>

<div class="row">
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
		<div class="row grid">
			<?php

			if (have_posts()) :
				// Start the loop.
				while (have_posts()) : the_post();

					/*
					 * Include the Post-Format-specific template for the content.
					 * If you want to override this in a child theme, then include a file
					 * called content-___.php (where ___ is the Post Format name) and that will be used instead.
					 */
					get_template_part('template-parts/content', get_post_format());

				endwhile;

			// If no content, include the "No posts found" template.
			else :

				get_template_part('template-parts/content', 'none');

			endif;
			?>
		</div>

		<?php if(get_the_posts_pagination()):
		?>
		<div class="pagination">
			<?php echo paginate_links(); ?>
		</div>
		<?php endif; ?>
	</div>

</div>