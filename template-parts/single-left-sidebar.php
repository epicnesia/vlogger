<?php
/**
 * The template for displaying all single posts and attachments
 * - Left sidebar layout
 *
 * @package WordPress
 * @subpackage Vlogger
 * @since Vlogger 1.0.4
 */

$active_sidebar = is_active_sidebar('vlogger-primary-sidebar'); ?>
		
			<!-- Start Left Sidebar Layout -->
					
				<?php if($active_sidebar) : ?>
					
				<aside class="col-lg-4 col-md-4 col-sm-12 col-xs-12 left">
					<?php get_sidebar(); ?>
				</aside>
					
				<?php endif; ?>
				
				<div class="<?php echo $active_sidebar ? 'col-lg-8 col-md-8 col-sm-12 col-xs-12 right' : 'col-lg-12 col-md-12 col-sm-12 col-xs-12'; ?>">
					
					<?php 
						
						while ( have_posts() ) : the_post();
						
							if ( is_singular() ) echo '<div class="row">';
							
							// Include the single post content template.
							get_template_part( 'template-parts/content', 'single' );
							
							if ( is_singular() ) echo '</div>';
							
							// If comments are open or we have at least one comment, load up the comment template.
							if ( comments_open() || get_comments_number() ) {
								comments_template();
							}
				
							if ( is_singular( 'post' ) ) {
								// Previous/next post navigation.
								the_post_navigation( array(
									'next_text' => '<span class="meta-nav" aria-hidden="true">' . __( 'Next', 'vlogger' ) . '</span> ' .
										'<span class="screen-reader-text">' . __( 'Next post:', 'vlogger' ) . '</span> ' .
										'<span class="post-title">%title</span>',
									'prev_text' => '<span class="meta-nav" aria-hidden="true">' . __( 'Previous', 'vlogger' ) . '</span> ' .
										'<span class="screen-reader-text">' . __( 'Previous post:', 'vlogger' ) . '</span> ' .
										'<span class="post-title">%title</span>',
								) );
							}
							
						endwhile;
						
					?>
					
				</div>
				
			<!-- End Left Sidebar Layout -->