<?php
/**
 * The template for displaying woocomerce content
 * - Right Sidebar layout
 *
 * @package WordPress
 * @subpackage Vlogger
 * @since Vlogger 1.0.4
 */

$active_sidebar = is_active_sidebar('vlogger-primary-sidebar'); ?>
			
			<!-- Start Right Sidebar / Default Layout -->
				
				<div class="<?php echo $active_sidebar ? 'col-lg-8 col-md-12 col-sm-12 col-xs-12 left' : 'col-lg-12 col-md-12 col-sm-12 col-xs-12'; ?>">
					
					<?php
						
							if ( is_singular() ) echo '<div class="row">';
					
							// Include the page content template.
							get_template_part( 'template-parts/content', 'woocomerce' );
							
							if ( is_singular() ) echo '</div>';
							
					?>
					
				</div>
					
				<?php if($active_sidebar) : ?>
					
				<aside class="col-lg-4 right">
					<?php get_sidebar(); ?>
				</aside>
					
				<?php endif; ?>
				
			<!-- End Right Sidebar / Default Layout -->