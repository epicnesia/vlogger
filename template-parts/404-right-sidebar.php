<?php
/**
 * The template for displaying 404 pages (not found)
 * - Right sidebar layout
 *
 * @package WordPress
 * @subpackage Vlogger
 * @since Vlogger 1.0.4
 */
 
$active_sidebar = is_active_sidebar('vlogger-primary-sidebar'); ?>
			
			<!-- Start Right Sidebar / Default Layout -->
				
				<div class="<?php echo $active_sidebar ? 'col-lg-8 col-md-12 col-sm-12 col-xs-12 left' : 'col-lg-12 col-md-12 col-sm-12 col-xs-12'; ?>">
					
					<header class="archive-header">
						<h2 class="page-title"><?php _e('Oops! That page can&rsquo;t be found.', 'vlogger'); ?></h2>
					</header><!-- .page-header -->
					<div class="row grid">
			
						<?php get_template_part('template-parts/content', 'none'); ?>
						
					</div>
					
				</div>
					
				<?php if($active_sidebar) : ?>
					
				<aside class="col-lg-4 right">
					<?php get_sidebar(); ?>
				</aside>
					
				<?php endif; ?>
				
			<!-- End Right Sidebar / Default Layout -->