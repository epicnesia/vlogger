<?php
/**
 * The template part for displaying custom-header
 * - Centered logo
 *
 * @package WordPress
 * @subpackage Vlogger
 * @since Vlogger 1.0.4
 */
 ?>
 
 		<!-- Start Site Branding -->
		<div class="row site-branding-wrapper">
			<div class="container">
				<div class="row">
					<div class="col-lg-12">
						<div class="row">
							<?php
							
								$image = wp_get_attachment_image_src( get_theme_mod( 'custom_logo' ) , 'full' );
								
								if ( $image[0] ) :
									
							?>
							
							<div class="centered">
								<a class="logo" href="<?php echo esc_url(home_url('/')); ?>" title="<?php echo esc_attr(get_bloginfo('name', 'display')); ?>" rel="home">
									<img src="<?php echo $image[0]; ?>" alt="<?php echo esc_attr(get_bloginfo('name', 'display')); ?>" />
								</a>
							</div>
							
							<?php

							else :
							?>
							
								<?php if(get_header_textcolor()!='blank') { ?>
								<div class="centered site-title">
									<a class="logo" href="<?php echo esc_url(home_url('/')); ?>" style="color:#<?php echo get_header_textcolor(); ?>!important;" title="<?php echo esc_attr(get_bloginfo('name', 'display')); ?>" rel="home">
										<?php bloginfo('name'); ?>
									</a>
								</div>
								<?php } ?>
							
							<?php

							endif;
							?>
							
							<?php
							$description = get_bloginfo( 'description', 'display' );
							if ( $description || is_customize_preview() ) : ?>
								<div class="centered site-description"><?php echo $description; ?></div>
							<?php endif; ?>
							
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- End Site Branding -->