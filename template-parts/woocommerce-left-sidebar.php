<?php
/**
 * The template for displaying woocomerce content
 * - Left Sidebar layout
 *
 * @package WordPress
 * @subpackage Vlogger
 * @since Vlogger 1.0.4
 */

$active_sidebar = is_active_sidebar('vlogger-primary-sidebar'); ?>
			
			<!-- Start Left Sidebar Layout -->
					
				<?php if($active_sidebar) : ?>
					
				<aside class="col-lg-4 left">
					<?php get_sidebar(); ?>
				</aside>
					
				<?php endif; ?>
				
				<div class="<?php echo $active_sidebar ? 'col-lg-8 col-md-12 col-sm-12 col-xs-12 right' : 'col-lg-12 col-md-12 col-sm-12 col-xs-12'; ?>">
					
					<?php
				
							if ( is_singular() ) echo '<div class="row">';
					
							// Include the page content template.
							get_template_part( 'template-parts/content', 'woocomerce' );
							
							if ( is_singular() ) echo '</div>';
							
					?>
					
				</div>
				
			<!-- End Left Sidebar Layout -->