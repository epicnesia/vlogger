<?php
/**
 * The template part for displaying custom-header
 * - One line logo and primary menu
 *
 * @package WordPress
 * @subpackage Vlogger
 * @since Vlogger 1.0.4
 */
 ?>
 
 		<div class="row site-branding-wrapper">
			<div class="container">
				<div class="row">
					<!-- Start Site Branding -->
					<div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
						<div class="row">
							<?php
							
								$image = wp_get_attachment_image_src( get_theme_mod( 'custom_logo' ) , 'full' );
								
								if ( $image[0] ) :
									
							?>
							<a class="logo" href="<?php echo esc_url(home_url('/')); ?>" title="<?php echo esc_attr(get_bloginfo('name', 'display')); ?>" rel="home">
								<img src="<?php echo $image[0]; ?>" alt="<?php echo esc_attr(get_bloginfo('name', 'display')); ?>" />
							</a>
							
							<?php

							else :
							?>
							
								<?php if(get_header_textcolor()!='blank') { ?>
								<span class="site-title">
									<a class="logo" href="<?php echo esc_url(home_url('/')); ?>" style="color:#<?php echo get_header_textcolor(); ?>!important;" title="<?php echo esc_attr(get_bloginfo('name', 'display')); ?>" rel="home">
										<?php bloginfo('name'); ?>
									</a>
								</span>
								<?php } ?>
							
							<?php
							$description = get_bloginfo( 'description', 'display' );
							if ( $description || is_customize_preview() ) : ?>
								<span class="site-description"><?php echo $description; ?></span>
							<?php endif; ?>
							
							<?php

							endif;
							?>
						</div>
					</div>
					<!-- End Site Branding -->
					
					<!-- Start Primary Navigation -->
					<nav class="col-lg-10 col-md-10">
						<?php
						wp_nav_menu(array('theme_location' => 'primary', 'sort_column' => 'menu_order', 'container_class' => 'primary-menu hidden-sm hidden-xs', 'menu_class' => 'sf-menu', 'fallback_cb' => 'default_menu'));
						?>
					</nav>
					<!-- End Primary Navigation -->
					
				</div>
			</div>
		</div>