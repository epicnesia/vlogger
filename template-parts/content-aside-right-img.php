<?php
/**
 * The template part for displaying content
 * Aside Post Formats
 * - Right featured image layout
 *
 * @package WordPress
 * @subpackage Vlogger
 * @since Vlogger 1.0.4
 */
 
 ?>
 <div id="post-<?php the_ID(); ?>" <?php post_class(vlogger_class_col(is_singular())); ?>>
	<div class="post-list">
		<div class="post-content <?php echo has_post_thumbnail() ? 'col-lg-6' : ''; ?>">
			<header class="post-header">
			<div class="post-meta">
				<p>Posted On <a href="<?php echo get_day_link(get_the_time('Y'), get_the_time('m'), get_the_time('d')); ?>"><?php echo get_the_date(); ?></a>, By <a href="<?php echo get_author_posts_url(get_the_author_meta('ID')); ?>"><?php echo get_the_author_meta('display_name') ?></a></p>
			</div>
		</header>
		<article class="post-article">
			<?php the_content(); ?>
		
			<?php

			wp_link_pages(array('before' => '<div class="page-links">' . esc_html__('Pages:', 'vlogger'), 'after' => '</div>', 'link_before' => '<span>', 'link_after' => '</span>', 'pagelink' => '%', 'echo' => 1));
			?>
		</article>
		</div>
		<?php
		
		if ( has_post_thumbnail() ) {
			?>
			<div class="post-list-thumb col-lg-6">
				<div class="row">
					<?php

					the_post_thumbnail(vlogger_thumbnail_name());
					?>
				</div>
			</div>
			<?php
			}
		?>
	</div>
</div>
