<?php 
/**
 * The template for displaying image attachments
 * - Right sidebar layout
 *
 * @package WordPress
 * @subpackage Vlogger
 * @since Vlogger 1.0.4
 */
 
 $active_sidebar = is_active_sidebar('vlogger-primary-sidebar'); ?>
 
			<!-- Start Right Sidebar / Default Layout -->
				
				<div class="<?php echo $active_sidebar ? 'col-lg-8 col-md-12 col-sm-12 col-xs-12 left' : 'col-lg-12 col-md-12 col-sm-12 col-xs-12'; ?>">
					
					<?php 
						
						while ( have_posts() ) : the_post();
							
					?>
					
					<div class="row">
					
						<div id="post-<?php the_ID(); ?>" <?php post_class( 'col-lg-12' ); ?>>
							
							<div class="post-content">
								
								<header class="post-header">
									
									<?php the_title('<h3 class="post-title">','</h3>'); ?>
									
									<?php vlogger_post_meta(); ?>
									
								</header>
								
								<article class="post-article">
									<?php echo wp_get_attachment_image( get_the_ID(), 'large' ); ?>
								</article>
								
								<footer class="post-footer">
									<?php
										// Retrieve attachment metadata.
										$metadata = wp_get_attachment_metadata();
										if ( $metadata ) {
											printf( '<div class="full-size-link"><span>%1$s </span><a href="%2$s">%3$s &times; %4$s</a></div>',
												esc_html_x( 'Full size', 'Used before full size attachment link.', 'vlogger' ),
												esc_url( wp_get_attachment_url() ),
												absint( $metadata['width'] ),
												absint( $metadata['height'] )
											);
										}
									?>
								</footer>
								
							</div>
							
						</div>
						
					</div>
					
					<?php
					
						// If comments are open or we have at least one comment, load up the comment template.
						if ( comments_open() || get_comments_number() ) {
							comments_template();
						}
							
						endwhile;
						
					?>
					
				</div>
					
				<?php if($active_sidebar) : ?>
					
				<aside class="col-lg-4 right">
					<?php get_sidebar(); ?>
				</aside>
					
				<?php endif; ?>
				
			<!-- End Right Sidebar / Default Layout -->