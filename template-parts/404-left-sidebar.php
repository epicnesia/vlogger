<?php
/**
 * The template for displaying 404 pages (not found)
 * - Left sidebar layout
 *
 * @package WordPress
 * @subpackage Vlogger
 * @since Vlogger 1.0.4
 */
 
$active_sidebar = is_active_sidebar('vlogger-primary-sidebar'); ?>
			
			<!-- Start Left Sidebar Layout -->
					
				<?php if($active_sidebar) : ?>
					
				<aside class="col-lg-4 left">
					<?php get_sidebar(); ?>
				</aside>
					
				<?php endif; ?>
				
				<div class="<?php echo $active_sidebar ? 'col-lg-8 col-md-12 col-sm-12 col-xs-12 right' : 'col-lg-12 col-md-12 col-sm-12 col-xs-12'; ?>">
					
					<header class="archive-header">
						<h2 class="page-title"><?php _e('Oops! That page can&rsquo;t be found.', 'vlogger'); ?></h2>
					</header><!-- .page-header -->
					<div class="row grid">
			
						<?php

						get_template_part('template-parts/content', 'none');
						?>
						
					</div>
					
				</div>
				
			<!-- End Left Sidebar Layout -->