<?php
/**
 * The template for displaying pages
 *
 * This is the template that displays all pages by default.
 * - Left Sidebar layout
 *
 * @package WordPress
 * @subpackage Vlogger
 * @since Vlogger 1.0.4
 */
 
$active_sidebar = is_active_sidebar('vlogger-primary-sidebar');

?>
			
			<!-- Start Left Sidebar Layout -->
					
				<?php if($active_sidebar) : ?>
					
				<aside class="col-lg-4 left">
					<?php get_sidebar(); ?>
				</aside>
					
				<?php endif; ?>
				
				<div class="<?php echo $active_sidebar ? 'col-lg-8 col-md-12 col-sm-12 col-xs-12 right' : 'col-lg-12 col-md-12 col-sm-12 col-xs-12'; ?>">
					
					<?php
						// Start the loop.
						while ( have_posts() ) : the_post();
						
							if ( is_singular() ) echo '<div class="row">';
				
							// Include the page content template.
							get_template_part( 'template-parts/content', 'page' );
							
							if ( is_singular() ) echo '</div>';
				
							// If comments are open or we have at least one comment, load up the comment template.
							if ( comments_open() || get_comments_number() ) {
								comments_template();
							}
				
							// End of the loop.
						endwhile;
					?>
					
				</div>
				
			<!-- End Left Sidebar Layout -->