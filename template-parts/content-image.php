<?php
/**
 * The template part for displaying content
 * Image Post Formats
 *
 * @package WordPress
 * @subpackage Vlogger
 * @since Vlogger 1.0
 */
?>
<a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
	<div id="post-<?php the_ID(); ?>" <?php post_class(vlogger_class_col(is_singular())); ?>>
		<?php the_content(); ?>
		<?php if(get_the_title()) : ?>
		<div class="post-content"><?php echo the_title(); ?></div>
		<?php endif; ?>
	</div>
</a>