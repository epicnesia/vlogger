<?php
/**
 * The template part for displaying content
 * Audio Post Format
 *
 * @package WordPress
 * @subpackage Vlogger
 * @since Vlogger 1.0
 */

$post_layout = get_theme_mod('vlogger_posts_layout');
?>

<div id="post-<?php the_ID(); ?>" <?php post_class(vlogger_class_col(is_singular())); ?>>
	<div class="audio-wrapper">
		<?php if($post_layout == '2') { ?>
			<div class="grid-layout">	
		<?php } ?>
		
		<?php

		if (!empty(vlogger_oembed_object())) {
			echo vlogger_oembed_object();
		} elseif (has_post_thumbnail()) {
			the_content();
			//the_post_thumbnail( $vlogger_thumbnail_name );
		}
	 	?>
		
		<?php if($post_layout == '2') { ?>
			</div>	
		<?php } ?>
	</div>
	<div class="post-content">
		<header class="post-header">
			
			<?php vlogger_category_list(); ?>
			
			<h3 class="post-title">
				<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
			</h3>
			
			<?php vlogger_post_meta(); ?>
			
		</header>
		<article class="post-article">
			<a class="btn read-more" href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>"><?php esc_html_e('Read More', 'vlogger'); ?></a>
		</article>
	</div>
</div>
