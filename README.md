# About
Vlogger is a beautiful blog or vlog template based on Bootstrap. It has many layouts that absolutely responsive, SEO friendly, WooCommerce integration and other powerful features. Vlogger is the right choice for you who want to attract more fans.

- **Theme Name**: Vlogger
- **Version**: 1.0.4
- **Tested up to**: WP 4.7.3
- **License**: GPLv2 or later
- **License URI**: http://www.gnu.org/licenses/gpl-2.0.html


**Tags**: one-column, two-columns, three-columns, left-sidebar, right-sidebar, custom-colors, custom-header, custom-background, custom-menu, custom-logo, featured-images, flexible-header, post-formats, sticky-post, footer-widgets, grid-layout, threaded-comments, blog, e-commerce, news


# Installation
1. In your admin panel, go to Appearance -> Themes and click the 'Add New' button.
2. Type in Vlogger in the search form and press the 'Enter' key on your keyboard.
3. Click on the 'Activate' button to use your new theme right away.
4. Navigate to Appearance > Customize in your admin panel and customize to taste.


# Credit
Unless otherwise specified, all the theme files, scripts and images are licensed under GPLv2 license

Vlogger theme uses:
* FontAwesome (http://fontawesome.io) licensed under the SIL OFL 1.1 (http://scripts.sil.org/OFL)
* Bootstrap (http://getbootstrap.com/) licensed under MIT license (https://github.com/twbs/bootstrap/blob/master/LICENSE)
* FlexSlider by WooThemes licensed under the GPLv2 license (http://www.gnu.org/licenses/gpl-2.0.html)
* jQuery Superfish Menu (https://github.com/joeldbirch/superfish) By Joel Birch licensed under the MIT and GPL licenses (http://www.opensource.org/licenses/mit-license.php & http://www.gnu.org/licenses/gpl.html)
* SlickNav Responsive Mobile Menu (http://slicknav.com/) by Josh Cope licensed under MIT (https://github.com/ComputerWolf/SlickNav/blob/master/MIT-LICENSE.txt)
* Masonry PACKAGED (http://masonry.desandro.com) by David DeSandro licensed under MIT (https://desandro.mit-license.org/)
* Modernizr (https://modernizr.com) licensed under MIT


# Changelog
**1.0.0**
- December 8, 2016 : Uploaded to Wordpress Directory, waiting for review

**1.0.1**
- March 01, 2017 : Fix some bugs

**1.0.2**
- March 02, 2017 : Updating some CSS and JS files

**1.0.3**
- March 02, 2017 : Remove modernizr.js

**1.0.4**
- March 02, 2017 : Refactoring