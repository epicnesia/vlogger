jQuery(document).ready(function() {

	//============================= Superfish Menu Setting ==================================
	jQuery('ul.sf-menu').superfish();

	// Slicknav for responsive
	jQuery('.top-menu ul.sf-menu').slicknav({
		label : 'TOP MENU',
		prependTo : '.top-menu-wrapper'
	});
	jQuery('.primary-menu ul.sf-menu').slicknav({
		label : 'MENU',
		prependTo : '.primary-menu-wrapper'
	});

	//============================= Youtube Playlist Setting ================================
	var player;

	jQuery('.ytpl-playlist-items:first-child a').addClass('active');

	jQuery('.ytpl-playlist-items a').click(function(e) {
		jQuery('.ytpl-playlist-items a').removeClass('active');
		var dataSrc = jQuery(this).attr('data-src');

		jQuery('.vlogger-ytpl-player').html('<div id="player"></div>');
		player = new YT.Player('player', {
			height : '440',
			width : '790',
			videoId : dataSrc,
			events : {
				'onReady' : onPlayerReady
			}
		});

		jQuery(this).addClass('active');
		e.preventDefault();

	});

	// This code loads the IFrame Player API code asynchronously.
	var tag = document.createElement('script');

	tag.src = "https://www.youtube.com/iframe_api";
	var firstScriptTag = document.getElementsByTagName('script')[0];
	firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

	// The API will call this function when the video player is ready.
	function onPlayerReady() {
		player.playVideo();
	}
	
	//============================= Footer Functions ================================
	var elCount = jQuery('#footer-widget div.widget').length;
	var maxEl = 4;
	var col = 12 / elCount;
	
	if(elCount < maxEl){
		jQuery('#footer-widget div.widget').addClass('col-lg-' + col + ' col-md-' + col + ' col-sm-6 col-xs-12');
	} else {
		jQuery('#footer-widget div.widget').addClass('col-lg-3 col-md-3 col-sm-6 col-xs-12');
	}

});
