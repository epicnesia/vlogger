<?php
/**
 * Template for displaying search forms
 * 
 * @package WordPress
 * @subpackage Vlogger
 * @since Vlogger 1.0
 */
?>

<form class="search-form" action="<?php echo esc_url(home_url('/')); ?>" method="get">
	<input type="search" class="search-field" name="s" placeholder="<?php echo esc_attr_x( 'Search &hellip;', 'placeholder', 'vlogger' ); ?>" />
	<button type="submit" class="search-submit"><i class="fa fa-search"></i></button>
</form>