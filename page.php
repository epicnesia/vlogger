<?php
/**
 * The template for displaying pages
 *
 * This is the template that displays all pages by default.
 *
 * @package WordPress
 * @subpackage Vlogger
 * @since Vlogger 1.0
 */
 
$active_sidebar = is_active_sidebar('vlogger-primary-sidebar');

get_header(); ?>

<?php if(get_theme_mod('vlogger_show_breadcrumbs') == 1) : ?>
<section id="breadcrumb" class="container-fluid">
	<div class="container">
		<div class="row">
			<?php vlogger_breadcrumbs(); ?>
		</div>
	</div>
</section>
<?php endif; ?>

<!-- Start Content -->
	<section id="content">
		
		<section class="container content-wrapper">
			
			<?php 
			
				echo $active_sidebar ? '' : '<div class="row">';
				
				get_template_part('template-parts/page', vlogger_get_content_layout());
				
				echo $active_sidebar ? '' : '</div>'; 
			
			?>
				
		</section>
<!-- End Content -->
		
<?php get_footer(); ?>