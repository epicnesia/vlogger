<?php
/**
 * The sidebar containing the primary widget area
 *
 * If no active widgets are in this sidebar, hide it completely.
 *
 * @package WordPress
 * @subpackage Vlogger
 * @since Vlogger 1.0
 */

if ( is_active_sidebar( 'vlogger-primary-sidebar' ) ) :
?>

<div id="sidebar" class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
	<?php dynamic_sidebar('vlogger-primary-sidebar'); ?>
</div>

<?php endif; ?>