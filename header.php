<?php
/**
 * The Header for our theme.
 *
 * Displays all of the <head> section and everything up till <section id="content">
 *
 * @package WordPress
 * @subpackage Vlogger
 * @since Vlogger 1.0
 */
?>

<!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
	<head>
		<meta charset="<?php bloginfo('charset'); ?>">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="profile" href="http://gmpg.org/xfn/11">
		<?php if ( is_singular() && pings_open( get_queried_object() ) ) :
		?>
		<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">
		<?php endif; ?>
		<?php wp_head(); ?>
	</head>

	<body <?php body_class(); ?>>

		<!-- Start Header -->
		<header id="header" class="container-fluid">
		
			<!-- Start Top Navigation -->
			<div class="row top-menu-wrapper">
				<nav class="container">
					<div class="row">
						<div class="top-menu col-lg-9 col-md-9 hidden-sm hidden-xs">
						<?php
	
						wp_nav_menu(array('theme_location' => 'top', 'sort_column' => 'menu_order', 'container_class' => 'row', 'menu_class' => 'sf-menu', 'fallback_cb' => 'default_menu'));
						?>
						</div>
						<?php
							if ( has_nav_menu( 'top' ) ) {
						?>
						
							<div class="top-search-form col-lg-3 col-md-3 hidden-sm hidden-xs">
								<?php get_search_form(); ?>
							</div>
						
						<?php
						}
						?> 
					</div>
				</nav>
			</div>
			<!-- End Top Navigation -->
			
			<?php 
			
				$header_layout = get_theme_mod('vlogger_header_layout');
			
				if( get_header_image() != '' ) :
					
					get_template_part('template-parts/header', 'image');
				
				elseif($header_layout == '2') :
					
					get_template_part('template-parts/header', 'banner');
				
				elseif($header_layout == '3') :
					
					get_template_part('template-parts/header', 'one-line');
				
				else :
					
					get_template_part('template-parts/header', 'centered');
					
				endif;
				
			?>
			
			<?php 
			
				if($header_layout != '3' || get_header_image() != '') : ?>

				<!-- Start Primary Navigation -->
				<div class="row primary-menu-wrapper">
					<nav class="container">
						<div class="row">
						<?php
		
						wp_nav_menu(array('theme_location' => 'primary', 'sort_column' => 'menu_order', 'container_class' => 'primary-menu hidden-sm hidden-xs', 'menu_class' => 'sf-menu', 'fallback_cb' => 'default_menu'));
						?>
						</div>
					</nav>
				</div>
				<!-- End Primary Navigation -->
				
			<?php endif; ?>
			
		</header>
		<!-- End Header -->
