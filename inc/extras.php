<?php
/**
 * Custom functions that act independently of the theme templates
 *
 * Eventually, some of the functionality here could be replaced by core features
 *
 * @package WordPress
 * @subpackage Vlogger
 * @since Vlogger 1.0
 */

/**
 * Vlogger slider
 * Displayed featured image on front page and blog
 *
 * @since Vlogger 1.0
 */
 
function vlogger_slider() {
	echo '<div class="flexslider">';
      echo '<ul class="slides">';

        $query = new WP_Query( array( 'cat' => get_theme_mod('vlogger_slider_cat', 1), 'posts_per_page' => get_theme_mod('vlogger_slider_number', 3) ) );
        if ($query->have_posts()) :
          while ($query->have_posts()) : $query->the_post();

          echo '<li><a href="'. get_permalink() .'">';
            if ( (function_exists( 'has_post_thumbnail' )) && ( has_post_thumbnail() ) ) :
              echo the_post_thumbnail('vlogger-slider');
            endif;

              echo '<div class="flexslider-caption">';
                  if ( get_the_title() != '' ) echo '<h2 class="flexslider-title">'. get_the_title().'</h2>';
                  if ( get_the_excerpt() != '' ) echo '<div class="flexslider-excerpt">' . get_the_excerpt() .'</div>';
              echo '</div>';
              echo '</a></li>';
              endwhile;
            endif;

      echo '</ul>';
    echo '</div>';
}

/**
 * Flexslider option 
 * 
 * @since Vlogger 1.0
 */
add_action('wp_footer', 'vlogger_flexslider_option', 20);
function vlogger_flexslider_option(){
	if( ( is_home() || is_front_page() ) && get_theme_mod('vlogger_slider_active') == 1 ) {

	?>
	
	<script type="text/javascript">
	
		jQuery('.flexslider').flexslider({
			animation : "<?php echo esc_html(get_theme_mod('vlogger_slider_effect', 'slider')); ?>",
			animationSpeed : "<?php echo esc_html(get_theme_mod('vlogger_slider_animation_speed', '800')); ?>"
		});
	
	</script>
	
	<?php
	}
}

/**
 * Vlogger Youtube Video Playlist
 * Displayed videos from YouTube
 *
 * @since Vlogger 1.0
 */
 
function vlogger_youtube_playlist() {
	echo '<div class="vlogger-ytpl">
			<div class="container">
				<div class="row">';
	
			echo '<header class="vlogger-ytpl-header">
					<h2 class="vlogger-ytpl-title">'.get_theme_mod('vlogger_ytpl_title', esc_html__('Check Out My Videos', 'vlogger')).'</h2>
				  </header>';
				  
			vlogger_video_list();
	
    echo '		</div>
    		</div>
    	</div>';
}

function vlogger_video_list() {
	
	$url = 'https://www.googleapis.com/youtube/v3/playlistItems?part=snippet&maxResults=50&playlistId='.get_theme_mod('vlogger_ytpl_playlist_id').'&key='.get_theme_mod('vlogger_ytpl_api_key');
	
	$get_result = wp_remote_get($url);
	
	$valid_result = json_decode($get_result['body']);
	
	if($get_result['response']['code'] == 200) :
	
	echo '<div class="vlogger-ytpl-player col-lg-8 col-md-8 col-sm-7 col-xs-12">';
		echo wp_oembed_get('http://www.youtube.com/watch?v='.$valid_result->items[0]->snippet->resourceId->videoId);
	echo '</div>';
			
	echo '<div class="vlogger-ytpl-playlist col-lg-4 col-md-4 col-sm-5 col-xs-12">';
		echo '<ul class="list-unstyled">';
		
		foreach($valid_result->items as $items) :
			echo '<li class="ytpl-playlist-items">
					<a href="#" data-src="'.$items->snippet->resourceId->videoId.'">
						<div class="playlist-data">
							<img class="playlist-thumb" src="'.$items->snippet->thumbnails->default->url.'" alt="'.$items->snippet->title.'" />
							<div class="playlist-descriptions-wrapper">
								<p class="playlist-title">'.$items->snippet->title.'</p>
								<p class="playlist-artist">'.$items->snippet->channelTitle.'</p>
							</div>
						</div>
					</a>
				</li>';
		endforeach;
		
	else :
		
		echo __('<h1 class="text-danger">Make sure you insert a valid YouTube playlist ID and valid YouTube data API key!</h1>', 'vlogger');
		
	endif;
		echo '</ul>';
	echo '</div>';
	
}

// Scan oembed object
function vlogger_oembed_object() {

    global $post;

    if ( $post && $post->post_content ) {

        global $shortcode_tags;
        // Make a copy of global shortcode tags - we'll temporarily overwrite it.
        $theme_shortcode_tags = $shortcode_tags;

        // The shortcodes we're interested in.
        $shortcode_tags = array(
            'video' => $theme_shortcode_tags['video'],
            'embed' => $theme_shortcode_tags['embed'],
            'audio' => $theme_shortcode_tags['audio'],
        );
        // Get the absurd shortcode regexp.
        $video_regex = '#' . get_shortcode_regex() . '#i';

        // Restore global shortcode tags.
        $shortcode_tags = $theme_shortcode_tags;

        $pattern_array = array( $video_regex );

        // Get the patterns from the embed object.
        if ( ! function_exists( '_wp_oembed_get_object' ) ) {
            include ABSPATH . WPINC . '/class-oembed.php';
        }
        $oembed = _wp_oembed_get_object();
        $pattern_array = array_merge( $pattern_array, array_keys( $oembed->providers ) );

        // Or all the patterns together.
        $pattern = '#(' . array_reduce( $pattern_array, function ( $carry, $item ) {
            if ( strpos( $item, '#' ) === 0 ) {
                // Assuming '#...#i' regexps.
                $item = substr( $item, 1, -2 );
            } else {
                // Assuming glob patterns.
                $item = str_replace( '*', '(.+)', $item );
            }
            return $carry ? $carry . ')|('  . $item : $item;
        } ) . ')#is';

        // Simplistic parse of content line by line.
        $lines = explode( "\n", $post->post_content );
        foreach ( $lines as $line ) {
            $line = trim( $line );
            if ( preg_match( $pattern, $line, $matches ) ) {
                if ( strpos( $matches[0], '[' ) === 0 ) {
                    $ret = do_shortcode( $matches[0] );
                } else {
                    $ret = wp_oembed_get( $matches[0] );
                }
                return $ret;
            }
        }
    }
}

/**
 * Converts a HEX value to RGB.
 * Inspired by Twenty_Sixteen twentysixteen_hex2rgb()
 * 
 * @since Vlogger 1.0
 *
 * @param string $color The original color, in 3- or 6-digit hexadecimal form.
 * 
 */
function vlogger_hex2rgb( $color, $opacity = 1 ) {
	$color = trim( $color, '#' );

	if ( strlen( $color ) === 3 ) {
		$r = hexdec( substr( $color, 0, 1 ).substr( $color, 0, 1 ) );
		$g = hexdec( substr( $color, 1, 1 ).substr( $color, 1, 1 ) );
		$b = hexdec( substr( $color, 2, 1 ).substr( $color, 2, 1 ) );
	} else if ( strlen( $color ) === 6 ) {
		$r = hexdec( substr( $color, 0, 2 ) );
		$g = hexdec( substr( $color, 2, 2 ) );
		$b = hexdec( substr( $color, 4, 2 ) );
	} else {
		return array();
	}

	return 'rgba( ' . $r . ', ' . $g . ', ' . $b . ', ' . $opacity . ')';
}

/**
 * Vlogger Copyright
 *
 * @since Vlogger 1.0
 */
function vlogger_copyright() {
	printf( esc_html__( 'Theme by %1$s Proudly powered by %2$s', 'vlogger' ) , '<a href="https://profiles.wordpress.org/taufikfredipratama" target="_blank"><strong>Epicnesia</strong></a>', '<a href="http://wordpress.org/" target="_blank"><strong>WordPress</strong></a>');
}
