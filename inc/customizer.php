<?php
/**
 * Vlogger Theme Customizer Class
 *
 * @package WordPress
 * @subpackage Vlogger
 * @since Vlogger 1.0
 */

class Vlogger_Customizer {
	
	static function register( $wp_customize ) {
		/**
		 * Add postMessage support for site title and description for the Theme Customizer.
		 *
		 * @param WP_Customize_Manager $wp_customize Theme Customizer object.
		 */
		$wp_customize->get_setting( 'blogname' )->transport         = 'postMessage';
		$wp_customize->get_setting( 'blogdescription' )->transport  = 'postMessage';
		$wp_customize->get_setting( 'header_textcolor' )->transport = 'postMessage';
		
		
		// Colors Settings
			$wp_customize->add_setting(
				'vlogger_primary_color',
				array(
					'default' 			=> '#CC181E',
					'sanitize_callback' => 'vlogger_sanitize_hexcolor'
				)
			);
				$wp_customize->add_control( new WP_Customize_Color_Control( 
					$wp_customize, 
					'vlogger_primary_color',
					array(
						'label'    => __( 'Primary Color', 'vlogger' ), 
						'section'  => 'colors',
						'priority' => 10,
					) 
				));
		
		// Header Layout Section
			$wp_customize->add_setting(
				'vlogger_header_layout',
				array(
					'default' => '1',
					'sanitize_callback' => 'vlogger_sanitize_number'
				)
			);
				$wp_customize->add_control(
					'vlogger_header_layout',
					array(
						'label' 	=> __( 'Header Layout', 'vlogger' ),
						'section' 	=> 'header_image',
						'type' 		=> 'radio',
						'choices' 	=> array(
							'1' => 'Header 1 - Centered Logo',
							'2' => 'Header 2 - Plus Banner',
							'3' => 'Header 3 - One line logo and primary menu'
						),
					)
				);
				
			$wp_customize->add_setting(
				'vlogger_header_banner_link',
				array(
					'default' 			=> '',
					'sanitize_callback' => 'esc_url'
				)
			);
				
				$wp_customize->add_control( 'vlogger_header_banner_link', 
	            	array(
	                    'type'      	=> 'text',
	                    'label'			=> __( 'Banner Link', 'vlogger' ),
	                    'section'		=> 'header_image',
                		'description' 	=> __('You can add a link to this banner', 'vlogger'),
	            	)
				);
			
			$wp_customize->add_setting(
				'vlogger_header_banner',
				array(
					'default' 			=> '',
					'sanitize_callback' => 'esc_url'
				)
			);
				
				$wp_customize->add_control( 
					new WP_Customize_Upload_Control( 
					$wp_customize, 
					'vlogger_header_banner', 
					array(
						'label'      => __( 'Banner Image', 'vlogger' ),
						'section'    => 'header_image',
					) ) 
				);
				
		// Slider Section
		$wp_customize->add_section( 
			'vlogger_slider_section', 
			array(
				'title'       => __( 'Slider', 'vlogger' ),
				'priority'    => 100,
				'capability'  => 'edit_theme_options',
			) 
		);
			$wp_customize->add_setting( 'vlogger_slider_active', 
				array(
                    'default' 			=> 0,
                    'sanitize_callback' => 'vlogger_sanitize_checkbox',
            	)
			);
	            $wp_customize->add_control( 'vlogger_slider_active', 
	            	array(
	                    'type'      => 'checkbox',
	                    'label'		=> __( 'Check to activate slider', 'vlogger' ),
	                    'section'	=> 'vlogger_slider_section'
	            	)
				);
			
			$wp_customize->add_setting( 'vlogger_slider_cat', 
				array(
                    'default' 			=> '',
                    'sanitize_callback' => 'vlogger_sanitize_slide_cat',
            	)
			);
				
	            $wp_customize->add_control( 'vlogger_slider_cat', 
	            	array(
	                    'type'      	=> 'select',
	                    'label'			=> __( 'Slider Category', 'vlogger' ),
	                    'section'		=> 'vlogger_slider_section',
                		'description' 	=> __('Select a category to be displayed on the slider', 'vlogger'),
	                    'choices'		=> vlogger_cat_list()
	            	)
				);
				
			$wp_customize->add_setting( 'vlogger_slider_number', 
				array(
                    'default' 			=> '3',
                    'sanitize_callback' => 'vlogger_sanitize_number',
            	)
			);
	            $wp_customize->add_control( 'vlogger_slider_number', 
	            	array(
	                    'type'      	=> 'text',
	                    'label'			=> __( 'Slider Posts Number', 'vlogger' ),
	                    'section'		=> 'vlogger_slider_section',
                		'description'	=> __('The number of posts displayed', 'vlogger')
	            	)
				);
				
			$wp_customize->add_setting( 'vlogger_slider_effect', 
				array(
                    'default' 			=> 'slider',
                    'sanitize_callback' => 'vlogger_sanitize_slider_effect'
            	)
			);
	            $wp_customize->add_control( 'vlogger_slider_effect', 
	            	array(
	                    'type'      	=> 'radio',
	                    'label'			=> __( 'Slider Effect', 'vlogger' ),
	                    'section'		=> 'vlogger_slider_section',
                		'description'	=> __('Select the slider effect', 'vlogger'),
	                    'choices'		=> array(
							'slider'	=> 'Slider',
							'fade'		=> 'Fade'
						)
	            	)
				);
				
			$wp_customize->add_setting( 'vlogger_slider_animation_speed', 
				array(
                    'default' 			=> '800',
                    'sanitize_callback' => 'vlogger_sanitize_number',
            	)
			);
	            $wp_customize->add_control( 'vlogger_slider_animation_speed', 
	            	array(
	                    'type'      	=> 'text',
	                    'label'			=> __( 'Slider Animation Speed', 'vlogger' ),
	                    'section'		=> 'vlogger_slider_section',
                		'description'	=> __('Animation speed in miliseconds', 'vlogger')
	            	)
				);
				
		// YouTube Playlist Section
		$wp_customize->add_section( 
			'vlogger_ytpl_section', 
			array(
				'title'       => __( 'Youtube Playlist', 'vlogger' ),
				'priority'    => 100,
				'capability'  => 'edit_theme_options',
			) 
		);	
			$wp_customize->add_setting( 'vlogger_ytpl_active', 
				array(
                    'default' 			=> '',
                    'sanitize_callback' => 'vlogger_sanitize_checkbox',
            	)
			);
	            $wp_customize->add_control( 'vlogger_ytpl_active', 
	            	array(
	                    'type'      	=> 'checkbox',
	                    'label'			=> __( 'Check to activate YouTube Playlist', 'vlogger' ),
	                    'section'		=> 'vlogger_ytpl_section'
	            	)
				);	
			$wp_customize->add_setting( 'vlogger_ytpl_title', 
				array(
                    'default' 			=> 'Check Out My Videos',
                    'transport'			=> 'postMessage',
                    'sanitize_callback' => 'sanitize_text_field'
            	)
			);
	            $wp_customize->add_control( 'vlogger_ytpl_title', 
	            	array(
	                    'type'      	=> 'text',
	                    'label'			=> __( 'YouTube Playlist Title', 'vlogger' ),
	                    'section'		=> 'vlogger_ytpl_section'
	            	)
				);	
			$wp_customize->add_setting( 'vlogger_ytpl_playlist_id', 
				array(
                    'default' 			=> '',
                    'sanitize_callback' => 'sanitize_text_field',
            	)
			);
	            $wp_customize->add_control( 'vlogger_ytpl_playlist_id', 
	            	array(
	                    'type'      	=> 'text',
	                    'label'			=> __( 'Insert your YouTube Playlist ID', 'vlogger' ),
	                    'section'		=> 'vlogger_ytpl_section'
	            	)
				);	
			$wp_customize->add_setting( 'vlogger_ytpl_api_key', 
				array(
                    'default' 			=> '',
                    'sanitize_callback' => 'sanitize_text_field',
            	)
			);
	            $wp_customize->add_control( 'vlogger_ytpl_api_key', 
	            	array(
	                    'type'      	=> 'text',
	                    'label'			=> __( 'Insert your YouTube Data API Key', 'vlogger' ),
	                    'section'		=> 'vlogger_ytpl_section',
	                    'description'	=> __('Here is how to get YouTube Data API key <a href="https://developers.google.com/youtube/v3/getting-started">https://developers.google.com/youtube/v3/getting-started</a>', 'vlogger')
	            	)
				);
				
		// Content Layout Section
		$wp_customize->add_section( 
			'vlogger_layout_section', 
			array(
				'title'       => __( 'Layout', 'vlogger' ),
				'priority'    => 100,
				'capability'  => 'edit_theme_options',
			) 
		);
			$wp_customize->add_setting( 'vlogger_posts_layout', 
				array(
                    'default' 			=> '1',
                    'sanitize_callback' => 'vlogger_sanitize_number',
            	)
			);
	            $wp_customize->add_control( 'vlogger_posts_layout', 
	            	array(
	                    'label'		=> __( 'Posts Layout', 'vlogger' ),
	                    'type' 		=> 'radio',
						'choices' 	=> array(
							'1' => 'Posts Default',
							'2' => 'Posts Grid',
							'3' => 'Posts List (Left Image)',
							'4' => 'Posts List (Right Image)'
						),
	                    'section'		=> 'vlogger_layout_section'
	            	)
				);
			$wp_customize->add_setting( 'vlogger_sidebar_position', 
				array(
                    'default' 			=> '1',
                    'sanitize_callback' => 'vlogger_sanitize_number',
            	)
			);
	            $wp_customize->add_control( 'vlogger_sidebar_position', 
	            	array(
	                    'label'		=> __( 'Posts Layout', 'vlogger' ),
	                    'type' 		=> 'radio',
						'choices' 	=> array(
							'1' => 'Right Sidebar',
							'2' => 'Left Sidebar',
						),
	                    'section'		=> 'vlogger_layout_section'
	            	)
				);
			
			$wp_customize->add_setting( 'vlogger_show_breadcrumbs', 
				array(
                    'default' 			=> '',
                    'sanitize_callback' => 'vlogger_sanitize_checkbox',
            	)
			);
	            $wp_customize->add_control( 'vlogger_show_breadcrumbs', 
	            	array(
	                    'type'      	=> 'checkbox',
	                    'label'			=> __( 'Check to show breadcrumbs', 'vlogger' ),
	                    'section'		=> 'vlogger_layout_section',
	            	)
				);
			
			$wp_customize->add_setting( 'vlogger_show_author_box', 
				array(
                    'default' 			=> '',
                    'sanitize_callback' => 'vlogger_sanitize_checkbox',
            	)
			);
	            $wp_customize->add_control( 'vlogger_show_author_box', 
	            	array(
	                    'type'      	=> 'checkbox',
	                    'label'			=> __( 'Check to show author box', 'vlogger' ),
	                    'section'		=> 'vlogger_layout_section',
	            	)
				);
				
		// Footer Section
		$wp_customize->add_section( 
			'vlogger_footer_section', 
			array(
				'title'       => __( 'Footer', 'vlogger' ),
				'capability'  => 'edit_theme_options',
			) 
		);
			$wp_customize->add_setting( 'vlogger_footer_text', 
				array(
                    'default' 			=> '',
                    'transport'			=> 'postMessage',
                    'sanitize_callback' => 'sanitize_text_field',
            	)
			);
	            $wp_customize->add_control( 'vlogger_footer_text', 
	            	array(
	                    'type'      	=> 'textarea',
	                    'label'			=> __( 'Footer Text', 'vlogger' ),
	                    'section'		=> 'vlogger_footer_section',
	            	)
				);
	}
	
	static function header_output() {
		// The live CSS
		
		?>
		<style type='text/css'>
			
			mark, ins, button:hover, button:focus, input[type="button"]:hover, input[type="button"]:focus, input[type="reset"]:hover, input[type="reset"]:focus, input[type="submit"]:hover, input[type="submit"]:focus, 
			.top-menu .sf-menu a:hover, .top-menu .sf-menu a:focus, .top-menu .sf-menu a:active, .primary-menu-wrapper .slicknav_menu, .pagination .page-numbers.current, .pagination .page-numbers:hover,
			.post-navigation .nav-previous a, .post-navigation .nav-next a, .flexslider .flexslider-caption .flexslider-title, .vlogger-ytpl-header:after, .post-content .post-article a.read-more, 
			.comment-list .reply .comment-reply-link:hover, .home .format-quote .post-content .post-article, .home .format-link .post-content, .archive .format-quote .post-content .post-article, 
			.archive .format-link .post-content, .widget-title:after, #footer-widget .search-submit {
				background-color:<?php echo get_theme_mod('vlogger_primary_color'); ?>;
			}
			
			<?php
			
				$rgb = vlogger_hex2rgb(sanitize_hex_color_no_hash(get_theme_mod('vlogger_primary_color')), '0.6');
				if(!is_array($rgb)){
			
			?>
					.flexslider .flexslider-caption .flexslider-title, .home .format-quote .post-content .post-article, .archive .format-quote .post-content .post-article {
						background-color: <?php echo $rgb ?>;
					}
			
			<?php } ?>
			
			a, a:hover, a:focus, a:active, .primary-menu .sf-menu a:hover, .primary-menu .sf-menu a:focus, .primary-menu .sf-menu a:active, .post-content .post-header ul.category-list .category-list-separator,
			.post-content .post-header .post-title a:hover, #footer a:hover {
				color:<?php echo get_theme_mod('vlogger_primary_color'); ?>;
			}
			
			.primary-menu .sf-menu a:hover, .primary-menu .sf-menu a:focus, .primary-menu .sf-menu a:active {
				border-top-color:<?php echo get_theme_mod('vlogger_primary_color'); ?>;
				border-bottom-color:<?php echo get_theme_mod('vlogger_primary_color'); ?>;
			}
			
			.sticky .post-content {
				border-color:<?php echo get_theme_mod('vlogger_primary_color'); ?>;
			}
			
		</style>
		<?php
		
	}
	
	static function live_preview() {
		// The live preview handler
		wp_enqueue_script( 'vlogger-custom-preview', get_template_directory_uri() . '/js/customizer.js', array( 'customize-preview', 'jquery' ), '20161213', true );
	}
	
}

add_action( 'customize_register' , array( 'Vlogger_Customizer' , 'register' ) );
add_action( 'wp_head' , array( 'Vlogger_Customizer' , 'header_output' ) );
add_action( 'customize_preview_init' , array( 'Vlogger_Customizer' , 'live_preview' ) );

/*
 * Vlogger Custom Sanitizer
 */
 // Sanitize checkbox
function vlogger_sanitize_checkbox( $input ) {
	if ( $input == 1 ) {
		return 1;
	} else {
		return '';
	}
}

// Sanitize category list
function vlogger_sanitize_slide_cat( $input ) {
    
    if ( array_key_exists( $input, vlogger_cat_list() ) ) {
        return $input;
    } else {
        return '';
    }
	
}

// Sanitize number
function vlogger_sanitize_number( $input ) {
    if ( isset( $input ) && is_numeric( $input ) ) {
        return $input;
    }
}

// Sanitize slider effect
function vlogger_sanitize_slider_effect( $input ) {
	$slider_effects = array('slider'=>'slider', 'fade'=>'fade');
    if ( array_key_exists( $input, $slider_effects ) ) {
        return $input;
    } else {
        return 'slider';
    }
}

// Sanitize Hex Color
function vlogger_sanitize_hexcolor($color) {
    if ($unhashed = sanitize_hex_color_no_hash($color))
        return '#' . $unhashed;
    return $color;
}

/*
 * Custom Scripts
 */
add_action( 'customize_controls_print_footer_scripts', 'vlogger_customizer_scripts' );

function vlogger_customizer_scripts() { ?>
<script type="text/javascript">
    jQuery(document).ready(function() {
        /* Header Layout. */
        jQuery('#customize-control-vlogger_header_banner, #customize-control-vlogger_header_banner_link').hide();
        jQuery('#customize-control-vlogger_header_layout input[name=_customize-radio-vlogger_header_layout]').click(function() {
            
            if(jQuery('#customize-control-vlogger_header_layout input[name=_customize-radio-vlogger_header_layout]:checked').val() == 2){
            	jQuery('#customize-control-vlogger_header_banner, #customize-control-vlogger_header_banner_link').fadeIn(500);
            } else {
            	jQuery('#customize-control-vlogger_header_banner, #customize-control-vlogger_header_banner_link').fadeOut(500);
            }
            
        });

        if (jQuery('#customize-control-vlogger_header_layout input[name=_customize-radio-vlogger_header_layout]:checked').val() == 2) {
            jQuery('#customize-control-vlogger_header_banner, #customize-control-vlogger_header_banner_link').show();
        }
        
        /* Slider settings. */
       jQuery('#customize-control-vlogger_slider_cat, #customize-control-vlogger_slider_number, #customize-control-vlogger_slider_effect, #customize-control-vlogger_slider_animation_speed').hide();
       jQuery('#customize-control-vlogger_slider_active input').click(function() {
            
            jQuery('#customize-control-vlogger_slider_cat, #customize-control-vlogger_slider_number, #customize-control-vlogger_slider_effect, #customize-control-vlogger_slider_animation_speed').fadeToggle(500);
            
        });
        
        if(jQuery('#customize-control-vlogger_slider_active input:checked').val() !== undefined){
        	jQuery('#customize-control-vlogger_slider_cat, #customize-control-vlogger_slider_number, #customize-control-vlogger_slider_effect, #customize-control-vlogger_slider_animation_speed').show();
       	}
    });
</script>
<?php
}