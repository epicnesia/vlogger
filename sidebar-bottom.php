<?php
/**
 * The sidebar containing the footer / bottom widget area
 *
 * If no active widgets are in this sidebar, hide it completely.
 *
 * @package WordPress
 * @subpackage Vlogger
 * @since Vlogger 1.0
 */

if ( is_active_sidebar( 'vlogger-footer-sidebar' ) ) :
?>

<div class="row">
	<div class="container">
		<div class="row">
			<div id="footer-widget" class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="row">
					<?php dynamic_sidebar('vlogger-footer-sidebar'); ?>
				</div>
			</div>
		</div>
	</div>
</div>
<?php endif; ?>