<?php

/**
 * =========================================================================================
 * Vlogger template functions and definitions
 *
 * @package WordPress
 * @subpackage Vlogger
 * @since Vlogger 1.0
 * =========================================================================================
 */
		
/**
* Set the content width based on the theme's design and stylesheet.
*/
if ( ! isset( $content_width ) ) {
	$content_width = is_active_sidebar('vlogger-primary-sidebar') == true ? 693 : 1099;
}

if (!function_exists('vlogger_setup')) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * @since Vlogger 1.0
	 */
	function vlogger_setup() {

		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 */
		load_theme_textdomain('vlogger', get_template_directory() . '/languages');

		// Add default posts and comments RSS feed links to head.
		add_theme_support('automatic-feed-links');

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support('title-tag');

		/*
		 * Enable support for custom logo.
		 *
		 *  @since Vlogger 1.2
		 */
		add_theme_support('custom-logo', array(
			'height' 		=> 270, 
			'width' 		=> 270, 
			'flex-height' 	=> true, 
		) );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link http://codex.wordpress.org/Function_Reference/add_theme_support#Post_Thumbnails
		 */
		add_theme_support('post-thumbnails');
		set_post_thumbnail_size(1600, 9999);

		add_image_size('vlogger-slider', 1600, 500, true); // Slider image
		add_image_size('vlogger-featured', 765, 400, true); // Full width featured image
		add_image_size('vlogger-featured-grid', 367, 250, true); // Grid layout featured image
		add_image_size('vlogger-featured-list', 390, 600, true); // List layout featured image

		// This theme uses wp_nav_menu() in two locations.
		register_nav_menus(array(
			'top' => __('Top Menu', 'vlogger'), 
			'primary' => __('Primary Menu', 'vlogger'), 
		) );

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support('html5', array(
			'search-form', 
			'comment-form', 
			'comment-list', 
			'gallery', 
			'caption', 
		) );

		/*
		 * Enable support for Post Formats.
		 *
		 * See: https://codex.wordpress.org/Post_Formats
		 */
		add_theme_support('post-formats', array(
			'aside', 
			'gallery', 
			'link', 
			'image', 
			'quote', 
			'status', 
			'video', 
			'audio'
		) );

		// Setup the WordPress core custom header feature.
		add_theme_support( 'custom-header', apply_filters( 'vlogger_custom_header_args', array(
			'default-image'          => '',
			'width'                  => 1600,
			'height'                 => 200,
			'flex-height'            => true,
			'flex-width'             => true,
			'default-text-color'	 => 'CC181E'
		) ) );

		// Setup the WordPress core custom background feature.
		add_theme_support( 'custom-background', apply_filters( 'vlogger_custom_background_args', array(
			'default-color' => 'F5F6F5'
		) ) );

		/*
		 * In order to allow partial refreshes of widgets in a theme’s sidebars
		 */
		add_theme_support('customize-selective-refresh-widgets');
	}

endif; // vlogger_setup
add_action('after_setup_theme', 'vlogger_setup');

/**
 * Registers a widget area.
 *
 * @link https://developer.wordpress.org/reference/functions/register_sidebar/
 *
 * @since Vlogger 1.0
 */
function vlogger_widgets_init() {
		
	register_sidebar(array(
		'name' 			=> __('Primary Sidebar', 'vlogger'), 
		'id' 			=> 'vlogger-primary-sidebar', 
		'description' 	=> __('Add widgets here to appear in your sidebar.', 'vlogger'), 
		'before_widget' => '<div id="%1$s" class="widget %2$s">', 
		'after_widget' 	=> '</div>', 
		'before_title' 	=> '<h5 class="widget-title">', 
		'after_title' 	=> '</h5>', 
	) );
					
	register_sidebar(array(
		'name' 			=> __('Footer Sidebar', 'vlogger'), 
		'id' 			=> 'vlogger-footer-sidebar', 
		'description' 	=> __('Add widgets here to appear in your footer.', 'vlogger'), 
		'before_widget' => '<div id="%1$s" class="widget %2$s">', 
		'after_widget' 	=> '</div>', 
		'before_title' 	=> '<h5 class="widget-title">', 
		'after_title' 	=> '</h5>', 
	) );
}

add_action('widgets_init', 'vlogger_widgets_init');

/**
 * Handles JavaScript detection.
 *
 * Adds a `js` class to the root `<html>` element when JavaScript is detected.
 *
 * @since Vlogger 1.0
 */
function vlogger_javascript_detection() {
	echo "<script>(function(html){html.className = html.className.replace(/\bno-js\b/,'js')})(document.documentElement);</script>\n";
}

add_action('wp_head', 'vlogger_javascript_detection', 0);

/**
 * Enqueues scripts and styles.
 *
 * @since Vlogger 1.0
 */
function vlogger_scripts() {

	// Add Bootstrap default CSS
	wp_enqueue_style('vlogger-bootstrap', get_template_directory_uri() . '/css/bootstrap.min.css', array(), '3.3.7');

	// Add Font Awesome stylesheet
	wp_enqueue_style('vlogger-font-awesome', get_template_directory_uri() . '/css/font-awesome.min.css', array(), '4.7.0');

	// Add Google Fonts
	wp_register_style('vlogger-fonts', '//fonts.googleapis.com/css?family=Open+Sans:400italic,400,600,700|Inconsolata:400,300,700');
	wp_enqueue_style('vlogger-fonts');

	// Add Superfish CSS
	wp_enqueue_style('vlogger-superfish', get_template_directory_uri() . '/css/superfish.css', array(), '1.7.9');

	// Add SlickNav CSS
	wp_enqueue_style('vlogger-slicknav', get_template_directory_uri() . '/css/slicknav.min.css', array(), '1.0.10');

	// Add Flexslider CSS
	if ((is_home() || is_front_page()) && get_theme_mod('vlogger_slider_active') == 1) {
		wp_enqueue_style('vlogger-flexslider', get_template_directory_uri() . '/css/flexslider.css', array(), '2.6.2');
	}

	// Theme stylesheet.
	wp_enqueue_style('vlogger-style', get_stylesheet_uri());

	if (is_singular() && comments_open() && get_option('thread_comments')) {
		wp_enqueue_script('comment-reply');
	}
	
	// Load html5shiv and Respond.js for IE
	wp_enqueue_script('vlogger-html5shiv-js', get_template_directory_uri() . '/js/html5shiv.min.js', array(), '3.7.2');
	wp_script_add_data('vlogger-html5shiv-js', 'conditional', 'lt IE 9' );
	wp_enqueue_script('vlogger-respond-js', get_template_directory_uri() . '/js/respond.min.js', array(), '1.4.2');
	wp_script_add_data('vlogger-respond-js', 'conditional', 'lt IE 9' );

	// Load bootstrap js
	wp_enqueue_script('vlogger-bootstrap-script', get_template_directory_uri() . '/js/bootstrap.min.js', array('jquery'), '3.3.7', true);

	// Load jQuery masonry
	if ((is_home() || is_front_page() || is_archive()) && get_theme_mod('vlogger_posts_layout') == 2) {
		wp_enqueue_script('vlogger-jquery-masonry', get_template_directory_uri() . '/js/masonry.pkgd.min.js', array('jquery'), '4.1.1', true);
		wp_enqueue_script('vlogger-jquery-masonry-set', get_template_directory_uri() . '/js/masonry.set.js', array('jquery'), '4.1.1', true);
	}

	// Load Superfish Menu script
	wp_enqueue_script('vlogger-hoverIntent-script', get_template_directory_uri() . '/js/hoverIntent.js', array('jquery'), '2013.03.11', true);
	wp_enqueue_script('vlogger-superfish-script', get_template_directory_uri() . '/js/superfish.min.js', array('jquery'), '1.7.9', true);

	// Load SlickNav Menu script
	wp_enqueue_script('vlogger-slicknav-script', get_template_directory_uri() . '/js/jquery.slicknav.min.js', array('jquery'), '1.0.10', true);

	// Load FlexSlider js
	if ((is_home() || is_front_page()) && get_theme_mod('vlogger_slider_active') == 1) {
		wp_enqueue_script('vlogger-flexslider-script', get_template_directory_uri() . '/js/jquery.flexslider.min.js', array('jquery'), '2.6.2', true);
	}

	// Load Vlogger functions scripts
	wp_enqueue_script('vlogger-script', get_template_directory_uri() . '/js/functions.js', array('jquery'), '1.0', true);

}

add_action('wp_enqueue_scripts', 'vlogger_scripts');

// WooCommerce Support Declaration
if ( ! function_exists( 'vlogger_woo_setup' ) ) :

// Sets up theme defaults and registers support for various WordPress features.
function vlogger_woo_setup() {
		
	// Enable support for WooCemmerce.
	add_theme_support( 'woocommerce' );

}
endif;
add_action( 'after_setup_theme', 'vlogger_woo_setup' );

/**
 * Vlogger Custom functions that act independently of the theme templates.
 *
 * @since Vlogger 1.0
 */
require get_template_directory() . '/inc/extras.php';

/**
 * Custom Vlogger template tags.
 *
 * @since Vlogger 1.0
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Vlogger Customizer additions.
 *
 * @since Vlogger 1.0
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Get the list of categories in array format for use on customizer
 *
 * @since Vlogger 1.0.5
 */
function vlogger_cat_list(){
	
	$vlogger_cat_list = array();
	$vlogger_cat_list_obj = get_categories();
	foreach ($vlogger_cat_list_obj as $cat) {
		$vlogger_cat_list[$cat -> cat_ID] = $cat -> cat_name;
	}
	
	return $vlogger_cat_list;
	
}

/**
 * Get the bootstrap's grid system class
 *
 * @since Vlogger 1.0.5
 */
function vlogger_class_col($single = false) {
	
	$sidebar_active = is_active_sidebar('vlogger-primary-sidebar');
	$post_layout = get_theme_mod('vlogger_posts_layout');
	
	$vlogger_class_col = 'col-lg-12';
	
	if($post_layout == '2'){
		$vlogger_class_col = $sidebar_active ? 'col-lg-6 col-md-6 col-sm-6 col-xs-12' : 'col-lg-4 col-md-4 col-sm-4 col-xs-12';
	}
	
	return $single ? 'col-lg-12' : $vlogger_class_col;
	
}

/**
 * Get the featured image's name
 *
 * @since Vlogger 1.0.5
 */
function vlogger_thumbnail_name() {
	
	$sidebar_active = is_active_sidebar('vlogger-primary-sidebar');
	$post_layout = get_theme_mod('vlogger_posts_layout');
	
	$vlogger_thumbnail_name = $sidebar_active ? 'vlogger-featured' : '';
	
	if($post_layout == '2'){
		$vlogger_thumbnail_name = 'vlogger-featured-grid';
	} elseif($post_layout == '3' || $post_layout == '4') {
		$vlogger_thumbnail_name = 'vlogger-featured-list';
	}
	
	return $vlogger_thumbnail_name;
	
}

/**
 * Vlogger get content layout.
 * - Left sidebar
 * - Right sidebar
 * - Full width
 *
 * @since Vlogger 1.0.5
 */
function vlogger_get_content_layout() {
	
	$sidebar_position_option = get_theme_mod('vlogger_sidebar_position', 1);
	$sidebar_active = is_active_sidebar('vlogger-primary-sidebar');
	
	$sidebar_position = 'full-width';
	
	if($sidebar_active){
		
		$sidebar_position = $sidebar_position_option == 1 ? 'right-sidebar' : 'left-sidebar';
		
	}
	
	return $sidebar_position;
	
}
