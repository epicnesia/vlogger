<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * e.g., it puts together the home page when no home.php file exists.
 *
 * Learn more: {@link https://codex.wordpress.org/Template_Hierarchy}
 *
 * @package WordPress
 * @subpackage Vlogger
 * @since Vlogger 1.0
 */

get_header(); ?>

	<!-- Start Content -->
	<section id="content">
		
		<?php if(!is_paged()): ?>
		
		<!-- Start Slider -->
		<section class="container-fluid">
			<div class="row">
				<?php if ( get_theme_mod('vlogger_slider_active') == 1 ) vlogger_slider(); ?>
			</div>
		</section>
		<!-- End Slider -->
		
		<!-- Start YoutTube Video Playlist -->
		<section class="container-fluid">
			<div class="row">
				<?php if ( get_theme_mod('vlogger_ytpl_active') == 1 ) vlogger_youtube_playlist(); ?>
			</div>
		</section>
		<!-- End YoutTube Video Playlist -->
		
		<?php endif; ?>
		
		<!-- Start Content -->
		<section class="container content-wrapper">
			
			<?php get_template_part('template-parts/index', vlogger_get_content_layout()); ?>
				
		</section>
		<!-- End Content -->

<?php get_footer(); ?>
