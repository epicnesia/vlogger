<?php
/**
 * The template for displaying image attachments
 *
 * @package WordPress
 * @subpackage Vlogger
 * @since Vlogger 1.0
 */

$active_sidebar = is_active_sidebar('vlogger-primary-sidebar');
 
get_header(); ?>

	<!-- Start Content -->
	<section id="content">
		
		<section class="container content-wrapper">
			
			<?php 
			
				echo $active_sidebar ? '' : '<div class="row">';
				
				get_template_part('template-parts/image', vlogger_get_content_layout());
				
				echo $active_sidebar ? '' : '</div>'; 
			
			?>
				
		</section>
		<!-- End Content -->
<?php get_footer(); ?>
